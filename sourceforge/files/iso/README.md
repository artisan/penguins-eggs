penguins-eggs
=============

## Penguin&#39;s eggs are generated and new birds are ready to fly...
[![sources](https://img.shields.io/badge/github-sources-blue)](https://github.com/pieroproietti/penguins-eggs)
[![blog](https://img.shields.io/badge/blog-penguin's%20eggs-blue)](https://penguins-eggs.net)
[![sources-documentation](https://img.shields.io/badge/sources-documentation-blue)](https://penguins-eggs.net/sources-documentation/index.html)
[![guide](https://img.shields.io/badge/guide-penguin's%20eggs-blue)](https://penguins-eggs.net/book/)
[![npm version](https://img.shields.io/npm/v/penguins-eggs.svg)](https://npmjs.org/package/penguins-eggs)
[![deb](https://img.shields.io/badge/deb-packages-orange)](https://sourceforge.net/projects/penguins-eggs/files/packages-deb)
[![iso](https://img.shields.io/badge/iso-images-orange)](https://sourceforge.net/projects/penguins-eggs/files/iso)


# Penguin's eggs remastered ISOs

All ISOs are based on Debian Buster, Ubuntu Focal, Linux Mint 19.x and Deepin 20 Beta. 

# user/password
* ```live/evolution```
* ```root/evolution```

All the ISOs include nodejs and eggs installed (.npm package), so you can update your eggs tool with the command:

```sudo eggs update```

# ISOs

I work mostly on Debian stable, so here you can find my personal versions and other examples.

# Debian
## buster

* **naked** - just the juice, without GUI. You can start here to build your revolution! (i386 and amd64)

* **less** - it's not naked, but just dressed with lxde-core and the tools to develop. (i386 and amd64)

* **debu**  - it is my personal version with cinnamon, mostly for development, but include common office tools; (amd64)

## bullseye

* **naked** - just the juice, without GUI. You can start here to build your revolution! (i386 and amd64)

* **lite** - just dressed with xfce, firefox, visual studio code, nodejs and stacer (amd64 and i386)

## proxmox-ve

* **naked-ve** - Proxmox VE 6.2 cli (need amd64 plus virtualization enabled)

* **live-ve** - Proxmox VE 6.2 with xfce, developer tools, virt-viewer (need amd64 plus virtualization enabled)

* **incubator** - Proxmox VE 6.2 with cinnamon, depeloper tools, office, gimp, etc; (need amd64 plus virtualization enabled)

All the other distro are non divided, but just a directory, the name is usually the distro version name. Example: focal, bionic, etc.

# Devuan 
* **beowulf** - like debu, but devual based

# Deepin
* **deepin** deepin 20 remastered

# Linux Mint
* **patricia** - Linux Mint 19.3 xfce, updated and remastered with eggs, without any modifications except for wallpapers. (amd64/i386)

* **ulyana** - Linux Mint 20 remastered with eggs, without any modifications except for wallpapers and develop tools. (amd64)

## Linux Mint Debian Edition 4 Debbie
* **debbie** - LMDE4 Debbie, remastered with eggs, without any modifications except for wallpapers and develop tools. (amd64/i386)

# Ubuntu
* **lubuntu-i386** - Lubuntu 18.04 remastered with eggs, w, updated and remastered with eggs, without any modifications except for wallpapers.

* **focal** - Ubuntu 20.04 focal gnome3, remastered with eggs, without any modifications. 

* **groovy** - Ubuntu 20.10 groovy. 


# UfficioZero

* **roma-i386** - UffucioZero Roma (x86), based on devuan beowulf

* **tropea** - UfficioZero Tropea (amd64)based on Linuxmint 20.0 ulyana

* **vieste** - UfficioZero Vieste (amd64) based on Linuxmint 19.3 tricia


You can create remix from yourself by downloading the originals, installing the system, installing eggs and start to creatie the ISOs.

# That's all Folks!
No need other configurations, penguins-eggs are battery included or better, as in the real, live is inside! :-D

## More informations

You can find more informations at [Penguin's eggs blog](https://penguins-eggs.net).

## Contacts
Feel free to contact [me](https://gitter.im/penguins-eggs-1/community?source=orgpage) or open an issue on [github](https://github.com/pieroproietti/penguins-eggs/issues).

* mail: piero.proietti@gmail.com

## Copyright and licenses
Copyright (c) 2017, 2020 [Piero Proietti](https://penguins-eggs.net/about-me.html), dual licensed under the MIT or GPL Version 2 licenses.
