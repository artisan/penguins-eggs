penguins-eggs
=============

## Penguin&#39;s eggs are generated and new birds are ready to fly...
[![sources](https://img.shields.io/badge/github-sources-blue)](https://github.com/pieroproietti/penguins-eggs)
[![blog](https://img.shields.io/badge/blog-penguin's%20eggs-blue)](https://penguins-eggs.net)
[![sources-documentation](https://img.shields.io/badge/sources-documentation-blue)](https://penguins-eggs.net/sources-documentation/index.html)
[![guide](https://img.shields.io/badge/guide-penguin's%20eggs-blue)](https://penguins-eggs.net/book/)
[![npm version](https://img.shields.io/npm/v/penguins-eggs.svg)](https://npmjs.org/package/penguins-eggs)
[![deb](https://img.shields.io/badge/deb-packages-orange)](https://sourceforge.net/projects/penguins-eggs/files/packages-deb)
[![iso](https://img.shields.io/badge/iso-images-orange)](https://sourceforge.net/projects/penguins-eggs/files/iso)


# Penguin's eggs remastered ISOs

# user/password
* ```live/evolution```
* ```root/evolution```

All the ISOs include nodejs and eggs installed (.npm package), so you can update your eggs tool with the command:

```sudo eggs update```

# Linux Mint
The purpose of Linux Mint is to produce a modern, elegant and comfortable operating system which is both powerful and easy to use.

Linux Mint is one of the most popular desktop Linux distributions and used by millions of people.

Some of the reasons for the success of Linux Mint are:

It works out of the box, with full multimedia support and is extremely easy to use.
It's both free of cost and open source.
It's community-driven. Users are encouraged to send feedback to the project so that their ideas can be used to improve Linux Mint.
Based on Debian and Ubuntu, it provides about 30,000 packages and one of the best software managers.
It's safe and reliable. Thanks to a conservative approach to software updates, a unique Update Manager and the robustness of its Linux architecture, Linux Mint requires very little maintenance (no regressions, no antivirus, no anti-spyware...etc).

* **debbie** - Linux Mint Debian Edition 4 (based on Debian buster)
* **tricia** - Linux Mint 19.3 (based on Ubuntu bionic)
* **ulyana** - Linux Mint 20 (based on Ubuntu focal)

## Linux Mint Debian Edition 4 Debbie
* **debbie** - LMDE4 Debbie, remastered with eggs, without any modifications except for wallpapers and develop tools.

## More informations

You can find more informations at [Linux Mint](https://linuxmint.com/).

# That's all Folks!
No need other configurations, penguins-eggs are battery included or better, as in the real, live is inside! :-D
