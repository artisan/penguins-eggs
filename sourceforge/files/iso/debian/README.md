penguins-eggs
=============

## Penguin&#39;s eggs are generated and new birds are ready to fly...
[![sources](https://img.shields.io/badge/github-sources-blue)](https://github.com/pieroproietti/penguins-eggs)
[![blog](https://img.shields.io/badge/blog-penguin's%20eggs-blue)](https://penguins-eggs.net)
[![sources-documentation](https://img.shields.io/badge/sources-documentation-blue)](https://penguins-eggs.net/sources-documentation/index.html)
[![guide](https://img.shields.io/badge/guide-penguin's%20eggs-blue)](https://penguins-eggs.net/book/)
[![npm version](https://img.shields.io/npm/v/penguins-eggs.svg)](https://npmjs.org/package/penguins-eggs)
[![deb](https://img.shields.io/badge/deb-packages-orange)](https://sourceforge.net/projects/penguins-eggs/files/packages-deb)
[![iso](https://img.shields.io/badge/iso-images-orange)](https://sourceforge.net/projects/penguins-eggs/files/iso)

# Debian remastered ISOs

All ISOs are based on Debian buster, Debian bullseye

# user/password
* ```live/evolution```
* ```root/evolution```

# Debian buster 

* **naked** - just the juice, without GUI. You can start here to build your revolution! (i386 and amd64)

* **less** - it's not naked, but just dressed with lxde-core and the tools to develop. (i386 and amd64)

* **debu**  - it is my personal version with cinnamon, mostly for development, but include common office tools; (amd64)

## Proxmox VE 6.2

## MX Linux 19.3

# Debian bullseye

* **naked** - just the juice, without GUI. You can start here to build your revolution! (i386 and amd64)

* **lite** - it's not naked, but just dressed with xfce4 and the tools to develop. (i386 and amd64)


* **naked** - just the juice, without GUI. You can start here to build your revolution! (amd64)

* **incubator** - it's your workstation for virtualization, dressed with cinnamon, libreoffice, gimp and tools. (amd64)
